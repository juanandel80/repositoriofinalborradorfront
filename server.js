//incluimos dotenv:fichero para incluir fichero de claves de cifrado - env
require('dotenv').config();

//LIBRERIAS - CONST
const express = require('express');
const app = express();

//PATAETRIZACIÓN: indico que aap se parsee como JSON
app.use(express.json());

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 //tenemos que incluir que acepte content-type
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}
//CONTROL: lo que se permite
app.use(enableCORS);

const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
//hay que definir la librería fs que hemos instalado previamente
//const fs = require('fs');
const port = process.env.PORT || 3000;


//API ESCUCHA PUERTO - escucha lo que entra por el puerto port
app.listen(port);
console.log("API escuchando en el puerto BIP BIP" + port);



//API V1 users - tiran de fichero - usuariospass.json
app.post("/apitechu/v1/login",authController.loginV1);
app.post("/apitechu/v1/logout/",authController.logoutV1);
app.get("/apitechu/v1/users",userController.getUsersV1);
app.post("/apitechu/v1/users",userController.createUsersV1);
app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);

//API V2 - atacan BBDD MongoDB
//APIs seguridad
app.post("/apitechu/v2/login",authController.loginV2);
app.post("/apitechu/v2/logout/:id",authController.logoutV2);
//APIs users
app.get("/apitechu/v2/users",userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUserByIdV2);
app.post("/apitechu/v2/users",userController.createUsersV2);
app.put("/apitechu/v2/users/:id",userController.modifyUserV2);
//APIs accounts
app.get("/apitechu/v2/users/accounts/:id",accountController.getAccountByUserIdV2);
app.post("/apitechu/v2/users/accounts/:id",accountController.createAccountByUserIdV2);
//APIs movements
app.post("/apitechu/v2/users/:userId/accounts/:IBAN/movements",accountController.createMovementByAccountV2);
app.get("/apitechu/v2/users/:userId/accounts/:IBAN/movements",accountController.getMovementByAccountV2);

//API externa
app.get("/apiexterna/RatesDivisa",accountController.getRatesDivisa);




//OTRAS PRUEBAS
//API añadiendo parámetros p1 y p2, salida consola params,query, headers, body
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");
    console.log("Parámetros:");
    console.log(req.params);

    console.log("Query String:");
    console.log(req.query);

    console.log("headers:")
    console.log(req.headers);

    console.log("Body:");
    console.log(req.body);
  }
)

//API escribir hello
app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU!"});
  }
)
