class MyFirstComponent extends Polymer.Element {

  static get is() {
    return 'my-first-component';
  }

  static get properties() {
    return {
      title:{
        type: String,
        value: '',
        notify: true
      },
      alternatetitle: {
        type: String,
        value: ''
      }
    };
  }

  _handleAlternateTitleClick() {
    this.alternatetitle = 'Titulo cambiado por manejador de evento'
  }

  _handleMessageSend() {
    this.alternatetitle = 'Titulo cambiado por hadleMessageSend'
  }
}

customElements.define(MyFirstComponent.is, MyFirstComponent);
