class MyLogin extends Polymer.Element {

  static get is() {
    return 'my-second-component';
  }

  static get properties() {
    return {
      title:{
        type: String,
        value: '',
        notify: true
      }

    };
  }

  _handleClick() {
    this.title='titulo cambiado por codigo'
    this.dispatchEvent(new CustomEvent('message-send', {buble: true,composed: true}))

  }
}

customElements.define(MyLogin.is, MyLogin);
