(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'home': '/home',
      'another': '/another',
      'users': '/users'
    }
  });

}(document));
