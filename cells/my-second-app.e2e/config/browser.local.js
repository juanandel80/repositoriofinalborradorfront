'use strict';

const baseConfig = require('./base').config;
const config = Object.assign({}, baseConfig, {
  host: 'localhost',
  port: 9999,
  selenium: {
    port: 9999
  },
  baseUrl: 'http://localhost:8001/build/composer-mock-local/novulcanize/index.html',
  environment: 'web',
  capabilities: [ {
    browserName: 'chrome'
  } ]
});

module.exports = { config };
