'use strict';

const poSample = require('../page_objects/sample');

module.exports = function() {

  this.Given(/^I am at the Initial page$/, function() {
    return poSample.start();
  });

  this.Then(/^I check if the element whit tag 'body' exists$/, function() {
    return poSample.waitForLoad();
  });

};