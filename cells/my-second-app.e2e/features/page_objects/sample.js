'use strict';


const View = {

  tagElement: 'body'

};

const entrypoint = {

  web() {
    return browser.url('/index.html');
  },

  app() {
    return browser;
  }

};


function start() {
  const environment = browser.options.environment || 'web';
  return entrypoint[environment]();
}


function waitForLoad() {
  return browser.waitForExist(View.tagElement,5000);
}


module.exports = {
  start,
  waitForLoad
};