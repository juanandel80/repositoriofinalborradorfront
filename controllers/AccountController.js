const io = require('../io');
const crypt = require('../crypt');
const authController = require('../controllers/AuthController');

//defino las nuevas constantes para los request de MongoDB con APIs
const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjapp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mlabBaseURLFixerio = "http://data.fixer.io/api/";
const mlabAPIKeYFixerio = "access_key=" + process.env.MLAB_API_KEY_FIXERIO;

//Cuentas asociadas al usuario logado
//pdte parte seguridad:
//  recuperar de cabeceras el tocken e ir a por el idusuario asociado al tocken
//  cambiar nombre API por: /apitechu/v2/users/accounts

function getAccountByUserIdV2 (req, res) {
  // console.log("Entramos en GET /apitechu/v2/accounts/:userid");
  console.log("Entramos en GET /apitechu/v2/users/accounts/:userid");
  console.log(req.params);

  var userId = req.params.id
  var query = 'q={"userId":' + userId + '}';

  console.log(query);

  //consultar si el usuario está logado

    var queryregistro = 'q={"id":'+req.params.id+'}'
    var httpClient = requestJson.createClient(mlabBaseURL);

    httpClient.get("user?" + queryregistro + '&' + mlabAPIKey,
       function(err, resMLab, body) {

         var logado = false;

         if (err) {
           res.status(500);
           res.send({"msg" : "Error accediendo a registro usuarios"});
         } else {
             var logado = body[0].logged;
             if (body.length = 0) {
               res.send({"msg" : "Error el usuario no existe"});
             } else {
               if (logado!==true) {
                 res.status(500);
                 res.send({"msg" : "Servicio no disponible"});
               }
             }
          }

  //si está logado permito continuar
        if (logado===true) {


          var httpClient = requestJson.createClient(mlabBaseURL);

          console.log("accounts?" + query + '&' + mlabAPIKey);

          httpClient.get("accounts?" + query + '&' + mlabAPIKey,
            function(err, resMLab, body) {

              if (err) {

                var response = {
                    "msg" : "Error obteniendo cuentas usuario"
                };
                res.status(500);
              } else {
                if (body.length > 0) {
                  var response = body;
                } else {
                  var response = {
                    "msg" : "Usuario sin cuentas asociadas"
                  };
                  res.status(404);
                }
              }
                res.send(response);
            }
          )
        }
      }
    )
  }


//  CREAR APIS nuevas:
//Alta cuenta:
function createAccountByUserIdV2(req, res){

  console.log("Entrando en POST /apitechu/v2/users/accounts/:userid");

  //meto valores por defecto por ahora: banco BBVA 0182 y oficina digital 1234
  var codigoBanco = "0182";
  var codigoSucursal = "1234";

  //como numero de cuenta usaré el idcuenta para no repetir el IBAN
  var querycount = 'c=true';
  var querylistorderasc = 's={"cuentaId":1}';
  var httpClient = requestJson.createClient(mlabBaseURL);
  var numeroRegistros = 0;

//consultar si el usuario está logado

  var queryregistro = 'q={"id":'+req.params.id+'}'

  httpClient.get("user?" + queryregistro + '&' + mlabAPIKey,
     function(err, resMLab, body) {

       var logado = false;

       if (err) {
         res.status(500);
         res.send({"msg" : "Error accediendo a registro usuarios"});
       } else {
           var logado = body[0].logged;
           if (body.length = 0) {
             res.send({"msg" : "Error el usuario no existe"});
           } else {
             if (logado!==true) {
               res.status(500);
               res.send({"msg" : "Servicio no disponible"});
             }
           }
        }

//si está logado permito continuar
      if (logado===true) {


        httpClient.get("accounts?" + querycount + '&' + mlabAPIKey,
          function(err, resMLab, numeroRegistros) {

          if (err) {
            res.status(500);
            res.send({"msg" : "Error obteniendo cuentas usuario"});
          } else {
            console.log("numeroRegistros:",numeroRegistros);
            if (numeroRegistros > 0) {
              console.log("entra aqui");
              console.log("querylistorderasc",querylistorderasc);

              var httpClient = requestJson.createClient(mlabBaseURL);
              httpClient.get("accounts?" + querylistorderasc + '&' + mlabAPIKey,
                function(err, resMLab, array) {

                  console.log("array:",array);
                  if (err) {
                    res.status(500);
                    res.send({"msg" : "Error obteniendo cuentas usuario"});
                  } else {

                    var numeroCuenta = "00000"+ array[numeroRegistros -1].cuentaId;
                    var digitoControl = io.CalculaDigitoControl(codigoBanco, numeroCuenta);
                    console.log("digitoControl:",digitoControl);
                    var codigoIBAN = io.CalcularIBAN(codigoBanco+codigoSucursal+digitoControl+numeroCuenta);
                    console.log("codigoIBAN:",codigoIBAN);
                    var IBAN = codigoIBAN+" "+codigoBanco+" "+codigoSucursal+" "+digitoControl+numeroCuenta.substr(0,2)+" "+numeroCuenta.substr(2,4)+" "+numeroCuenta.substr(6,10);

                    console.log("IBAN:",IBAN.substr(0,24));

                    //alta nuevo registro

                    var newAccount = {
                      "cuentaId" : array[numeroRegistros -1].cuentaId + 1,
                      "userId" : parseInt(req.params.id),
                      "IBAN" : IBAN.substr(0,24),
                      "balance" : 0
                    };

                    console.log("newAccount:",newAccount);

                    var httpClient = requestJson.createClient(mlabBaseURL);

                    httpClient.post("accounts?" + mlabAPIKey, newAccount,
                      function(err, resMLab) {

                        if (err) {
                          res.status(500);
                          res.send({"msg" : "Error en alta de cuenta"});
                        }else{
                          console.log("Usuario guardado con exito");
                          res.status(201);
                          res.send({"msg" : "Alta de cuenta registrada con éxito", "IBAN" : newAccount.IBAN});
                        }
                      }
                    )
                  }
                }
              )
           }else{

              var numeroCuenta = "0000000001";
              var digitoControl = io.CalculaDigitoControl(codigoBanco, numeroCuenta);
              console.log("digitoControl:",digitoControl);
              var codigoIBAN = io.CalcularIBAN(codigoBanco+codigoSucursal+digitoControl+1);
              console.log("codigoIBAN:",codigoIBAN);
              var IBAN = codigoIBAN+" "+codigoBanco+" "+codigoSucursal+" "+digitoControl+numeroCuenta.substr(0,2)+" "+numeroCuenta.substr(2,4)+" "+numeroCuenta.substr(6,10);

              console.log("IBAN:",IBAN.substr(0,24));

              var newAccount = {
                "cuentaId" : 1,
                "userId" : parseInt(req.params.id),
                "IBAN" : IBAN.substr(0,24),
                "balance" : 0
              };

              console.log("newAccount:",newAccount);

              var httpClient = requestJson.createClient(mlabBaseURL);

              httpClient.post("accounts?" + mlabAPIKey, newAccount,
                function(err, resMLab) {

                  if (err) {
                    res.status(500);
                    res.send({"msg" : "Error en alta de cuenta"});
                  }else{
                    console.log("Usuario guardado con exito");
                    res.status(201);
                    res.send({"msg" : "Alta de cuenta registrada con éxito", "IBAN" : newAccount.IBAN});
                  }
                }
              )
            }
            }
          }
        )
      }
    }
  )
}


//Alta movimientos cuenta

function createMovementByAccountV2 (req, res) {

  console.log("Entramos en POST /apitechu/v2/users/:userId/accounts/:IBAN/movements");
  console.log(req.params.IBAN);
  console.log("req.body1:",req.body);
  console.log(req.body);
  console.log(req.params);

  var queryregistro = 'q={"id":'+req.params.userId+'}';
  var httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("user?" + queryregistro + '&' + mlabAPIKey,
     function(err, resMLab, body) {

     var logado = false;
     var existe = false;

     if (err) {
       res.status(500);
       res.send({"msg" : "Error al acceder a usuario"});
     } else {
         if (body.length > 0) {
           console.log("el usuario exite:", body);
           existe = true;
           var logado = body[0].logged;
           console.log("logado:",logado);
           if (logado!==true) {
             logado = false;
             res.status(500);
             res.send({"msg" : "Servicio no disponible"});
           }
         } else {
           var resultado = "no encontrado";
           res.send({"msg" : "No existe usuario"});
         }
      }
      if (logado === true & existe === true) {
//hasta aquí el usuario está logado
//Siguiente : buscar el accountId con el IBAN

        console.log("IBAN IBAN1:",req.params.IBAN);
        if ((req.params.IBAN.substr(5,1)) === " ") {
          IBAN = req.params.IBAN;
        } else {
          IBAN = req.params.IBAN.substr(0,4)+" "+req.params.IBAN.substr(4,4)+" "+req.params.IBAN.substr(8,4)+" "+req.params.IBAN.substr(12,4)+" "+req.params.IBAN.substr(16,4);
        }
        console.log("IBAN IBAN2:",IBAN);

        var queryregistro = 'q={"$and": [{"userId":'+req.params.userId+'},{"IBAN":"'+IBAN+'"}]}';
        console.log(queryregistro);
        var httpClient = requestJson.createClient(mlabBaseURL);

        httpClient.get("accounts?" + queryregistro + '&' + mlabAPIKey,
           function(err, resMLab, body) {
             console.log("body:",body);
             if (err) {
               res.status(500);
               res.send({"msg" : "Error al acceder a cuentas"});
             } else {
                 if (body.length > 0) {
                    var accountId = body[0].cuentaId;
                    var balance = parseInt(body[0].balance);
                    console.log("cuenta encontrata:",accountId);
                    console.log("balance1:",balance);
                 } else {
                    res.status(409);
                    res.send({"msg" : "No existe cuenta"});
                 }
              }
              if (accountId > 0){

                console.log("balance2:",balance);
                console.log("req.body:",req.body);
                console.log("req.body.import:",req.body.import);
                var balance = balance + parseInt(req.body.import);
                console.log("balance3:",balance);
                var putBody = '{"$set":{"balance":'+balance+'}}';
                console.log(putBody);
              //el JSON.parse se pone para que reconozca putBody y no lo trate como cadena de caracteres
                httpClient.put("accounts?" + queryregistro + '&' + mlabAPIKey, JSON.parse(putBody),
                  function(err, resMLabPUT, bodyPUT) {
                    if (err) {
                      res.status(500);
                      res.send({"msg" : "Error actualizando balance cuenta"});
                    } else {
//ahora voy a localizar el ultimo movementId para insertar el siguiente
                      var querycount = 'c=true';

                      var httpClient = requestJson.createClient(mlabBaseURL);

                      httpClient.get("movements?" + querycount + '&' + mlabAPIKey,
                         function(err, resMLab, numeroRegistros) {

                            if (err) {
                              res.status(500);
                              res.send({"msg" : "Error obteniendo número de movimientos"});
                            } else {

                                var newMovement = {
                                  "accountId" : accountId,
                                  "IBAN" : IBAN,
                                  "movementId" : numeroRegistros+1,
                                  "concept" : req.body.concept,
                                  "import" : req.body.import,
                                  "importUSD" : req.body.importUSD,
                                  "actualBalance" : balance,
                                  "movementDate" : req.body.movementDate,
                                  "movementHour" : req.body.movementHour,
                                  "phone" : req.body.phone
                                };

                                console.log("newMovement:",newMovement);

                                var httpClient = requestJson.createClient(mlabBaseURL);

                                httpClient.post("movements?" + mlabAPIKey, newMovement,
                                  function(err, resMLab) {

                                    if (err) {
                                      res.status(500);
                                      res.send({"msg" : "Error en alta de movimiento"});
                                    }else{
                                      console.log("Movimiento guardado con exito");
                                      res.status(201);
                                      res.send({"msg" : "Alta de movimiento", "movimientoId" : newMovement.movementId});
                                    }
                                  }
                                )
                            }
                         }
                       )
                    }
                  }
                )
              }
           }
         )
      }
    }
  )
}



// Consulta movimientos cuenta:
// /apitechu/v2/users/:userId/accounts/:IBAN/movements/

function getRatesDivisa (req, res) {

  //API externa cambio divisa
    var httpClient = requestJson.createClient(mlabBaseURLFixerio);
    httpClient.get("latest?"+mlabAPIKeYFixerio,
     function(err, resMLab, body) {
      //  console.log(body);
      res.send(body);
    })
}
function getMovementByAccountV2 (req, res) {

  console.log("Entramos en GET /apitechu/v2/users/:userId/accounts/:IBAN/movements");

//API externa cambio divisa
  // var httpClient = requestJson.createClient(mlabBaseURLFixerio);
  // httpClient.get("latest?"+mlabAPIKeYFixerio,
  //  function(err, resMLab, body) {
  //   //  console.log(body);
  //   var cambioUSD = body.rates.USD;



  //Validación 1: primero revisar si el usuario existe y está logado
    var queryregistro = 'q={"id":'+req.params.userId+'}';
    var httpClient = requestJson.createClient(mlabBaseURL);


    httpClient.get("user?" + queryregistro + '&' + mlabAPIKey,
       function(err, resMLab, body) {

       var logado = false;
       var existe = false;


       if (err) {
         res.status(500);
         res.send({"msg" : "Error al acceder a usuario"});
       } else {
           if (body.length > 0) {
             console.log("el usuario existe:", body);
             existe = true;
             var logado = body[0].logged;
             console.log("logado:",logado);
             if (logado!==true) {
               logado = false;
               res.status(500);
               res.send({"msg" : "Servicio no disponible"});
             }
           } else {
             var resultado = "no encontrado";
             res.send({"msg" : "No existe usuario"});
           }
        }
        if (logado === true & existe === true) {
          //Siguiente : buscar el accountId con el IBAN

            console.log("IBAN IBAN1:",req.params.IBAN);
            if ((req.params.IBAN.substr(5,1)) === " ") {
              IBAN = req.params.IBAN;
            } else {
              IBAN = req.params.IBAN.substr(0,4)+" "+req.params.IBAN.substr(4,4)+" "+req.params.IBAN.substr(8,4)+" "+req.params.IBAN.substr(12,4)+" "+req.params.IBAN.substr(16,4);
            }
            console.log("IBAN IBAN2:",IBAN);

            var queryregistro = 'q={"$and": [{"userId":'+req.params.userId+'},{"IBAN":"'+IBAN+'"}]}';

            var httpClient = requestJson.createClient(mlabBaseURL);

            httpClient.get("accounts?" + queryregistro + '&' + mlabAPIKey,
              function(err, resMLab, body) {
                 console.log("body:",body);
                 if (err) {
                   res.status(500);
                   res.send({"msg" : "Error al acceder a cuentas"});
                 } else {
                     if (body.length > 0) {
                        var accountId = body[0].cuentaId;
                        console.log("cuenta encontrata:",accountId);
                     } else {
                        res.status(409);
                        res.send({"msg" : "No existe cuenta"});
                     }
                  }
                  if (accountId > 0){

                    //recuperamos los movimientos y ordena por fecha y hora
                    var query = 'q={"accountId":'+accountId+'}+&s={"movementDate":-1}&s={"movementHour":-1}';
                    var httpClient = requestJson.createClient(mlabBaseURL);
                    httpClient.get("movements?" + query + '&' + mlabAPIKey,
                       function(err, resMLab, body) {

                          if (err) {
                            res.status(500);
                            res.send({"msg" : "Error obteniendo número de movimientos"});
                          } else {
                            if (body.length>0) {
                              res.send(body);
                            }else{
                              console.log("no hay movimientos");
                              res.status(204);
                              res.send({"msg" : "sin movimientos"});
                            }
                          }
                        }
                      )

                  }
                }
              )
        }
      }
    )
  // })
}



//---------------------------------------------------------------------
// control canal: movimientos de la cuenta del usuario logado
//---------------------------------------------------------------------

module.exports.getAccountByUserIdV2 = getAccountByUserIdV2;
module.exports.createAccountByUserIdV2 = createAccountByUserIdV2;
module.exports.createMovementByAccountV2 = createMovementByAccountV2;
module.exports.getMovementByAccountV2 = getMovementByAccountV2;
module.exports.getRatesDivisa = getRatesDivisa;
