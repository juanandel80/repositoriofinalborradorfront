    const fs = require('fs');

//función escribir sobre fichero usuariospass - usuarios + password

    function writeUserDataToFile (data) {
      var jsonUserData = JSON.stringify(data);

      fs.writeFile("./usuariospass.json", jsonUserData, "utf8",
        function(err) {
          if (err) {
            console.log(err);
          } else {
            console.log("Datos escritos en el fichero");
          }
        }
      );

    }

// Calculate the checksum and assemble the IBAN.

    function CalcularIBAN(account)
    {
      //var checksum = ChecksumIBAN("ES" + "00" + account);


      var iban = "ES" + "00" + account;
      var code     = iban.substring(0, 2);
      var checksum = iban.substring(2, 4);
      var bban     = iban.substring(4);

      // Assemble digit string
      var digits = "";
      for (var i = 0; i < bban.length; ++i)
      {
        var ch = bban.charAt(i).toUpperCase();
        if ("0" <= ch && ch <= "9")
          digits += ch;
        else
          digits += convertirMayusculasNumeros(ch);
      }
      for (var i = 0; i < code.length; ++i)
      {
        var ch = code.charAt(i);
        digits += convertirMayusculasNumeros(ch);
      }
      digits += checksum;

      // Calculate checksum
      checksum = 98 - modulo97(digits);
      //return rellenarCeros("" + checksum, 2);
      //return "ES" + checksum;

      return "ES" + rellenarCeros("" + checksum, 2);
    }

// Calculate 2-digit checksum of an IBAN.
    function ChecksumIBAN(iban)
    {
      var code     = iban.substring(0, 2);
      var checksum = iban.substring(2, 4);
      var bban     = iban.substring(4);

      // Assemble digit string
      var digits = "";
      for (var i = 0; i < bban.length; ++i)
      {
        var ch = bban.charAt(i).toUpperCase();
        if ("0" <= ch && ch <= "9")
          digits += ch;
        else
          digits += convertirMayusculasNumeros(ch);
      }
      for (var i = 0; i < code.length; ++i)
      {
        var ch = code.charAt(i);
        digits += convertirMayusculasNumeros(ch);
      }
      digits += checksum;

      // Calculate checksum
      checksum = 98 - modulo97(digits);
      return rellenarCeros("" + checksum, 2);
    }

// Modulo 97 for huge numbers given as digit strings.
    function modulo97(digit_string)
    {
      var m = 0;
      for (var i = 0; i < digit_string.length; ++i)
        m = (m * 10 + parseInt(digit_string.charAt(i))) % 97;
      return m;
    }

// Convert a capital letter into digits: A -> 10 ... Z -> 35 (ISO 13616).
    function convertirMayusculasNumeros(ch)
    {
      var capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      for (var i = 0; i < capitals.length; ++i)
        if (ch == capitals.charAt(i))
          break;
      return i + 10;
    }

// Fill the string with leading zeros until length is reached.
    function rellenarCeros(s, l)
    {
      while (s.length < l)
        s = "0" + s;
      return s;
    }

//función cálculo dígito control cta formato CCC

    function CalculaDigitoControl(Banco, Cuenta) {
      Pesos= new Array(6,3,7,9,10,5,8,4,2,1);
      var result ='';
      var iTemp =0;

	//     if ((document.formcalculadora.codigoBanco.value=="") || (document.formcalculadora.codigoSucursal.value=="") || (document.formcalculadora.numeroCuenta.value==""))
	//  { alert("Por favor, introduzca todos los datos.");
	//    return (''); }

      console.log("Entrando CalculaDigitoControl");
      console.log("Banco",Banco);
      console.log("Cuenta",Cuenta);


      for (var n=0;n<=7;n++){
         iTemp  = iTemp + Banco.substr(7 - n, 1) * Pesos[n];
      }
      result = 11 - iTemp % 11;
      if (result > 9){
        result=1-result % 10;
      }
      iTemp=0;
      for (var n=0;n<=9;n++){
         iTemp  = iTemp + Cuenta.substr(9 - n, 1) * Pesos[n];
      }
      iTemp =11 - (iTemp % 11);
      if (iTemp > 9){
       iTemp =1-(iTemp % 10);
      }
      result=result*10+iTemp;

  	  if (result.toString().length == 1){
  	  	result = '0'+result;
  	  }

      console.log("resultado dentro:",result);

      return(result);
    }










//esta instrucción permite exportar la función y disponerla en otros .js

    module.exports.writeUserDataToFile = writeUserDataToFile;
    module.exports.CalculaDigitoControl = CalculaDigitoControl;
    module.exports.CalcularIBAN = CalcularIBAN;
