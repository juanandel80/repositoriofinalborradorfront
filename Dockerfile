# Dockerfile APITECHU

# Imagen raiz
FROM node

# Carpeta trabajo - es esta
WORKDIR /apitechu

# Añado archivos de mi aplicacion a imagen
ADD . /apitechu

# Instalo los paquetes necesarios
RUN npm install

# Abrir el puerto de la API (para poder acceder desde exterior)
EXPOSE 3000

# Comando de inicialización (para poner a funcionar la API)
CMD ["npm", "start"]
