const bcrypt = require('bcrypt');

//funcion que encripta
function hash(data) {
  console.log("Hashing data");

  return bcrypt.hashSync(data, 10);
}

//funcion que compara HASH
function checkpassword(sentPassword, userHashPassword) {
  console.log("Checking password");

  return bcrypt.compareSync(sentPassword, userHashPassword);
}

module.exports.hash = hash;
module.exports.checkpassword = checkpassword;
